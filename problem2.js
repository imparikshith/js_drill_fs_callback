const fs=require('node:fs');
function fsCallbackMethods(){
    fs.readFile('./lipsum_1.txt','utf-8',(error,data)=>{
        if(error){
            console.log(error);
        }
        else{
            console.log(data);
            fs.writeFile('./uppercaselipsum.txt',data.toUpperCase(),(error)=>{
                if(error){
                    console.log(error);
                }
                else{
                    console.log('uppercaselipsum.txt is created and contents are added in it.');
                    fs.writeFile('./filenames.txt','./uppercaselipsum.txt\n',(error)=>{
                        if(error){
                            console.log(error);
                        }
                        else{
                            console.log('filenames.txt is created and uppercaselipsum.txt is added in it');
                            fs.readFile('./uppercaselipsum.txt','utf-8',(error,data)=>{
                                if(error){
                                    console.log(error);
                                }
                                else{
                                    fs.writeFile('./lowercaselipsum.txt',splitIntoSentences(data.toLowerCase()),(error)=>{
                                        if(error){
                                            console.log(error);
                                        }
                                        else{
                                            console.log('lowercaselipsum.txt is created and contents are added in it.');
                                            fs.writeFile('./filenames.txt','./lowercaselipsum.txt\n',{flag:'a'},(error)=>{
                                                if(error){
                                                    console.log(error);
                                                }
                                                else{
                                                    console.log('lowercaselipsum.txt is added in filenames.txt');
                                                    fs.readFile('lowercaselipsum.txt','utf-8',(error,data)=>{
                                                        if(error){
                                                            console.log(error);
                                                        }
                                                        else{
                                                            let arrlipsum=data.split('\n').sort();
                                                            const sortedContent=arrlipsum.join('\n');
                                                            fs.writeFile('./sortedlipsum.txt',sortedContent,(error)=>{
                                                                if(error){
                                                                    console.log(error);
                                                                }
                                                                else{
                                                                    console.log('sortedlipsum.txt is created and contents are added in it.');
                                                                    fs.writeFile('./filenames.txt','./sortedlipsum.txt',{flag:'a'},(error)=>{
                                                                        if(error){
                                                                            console.log(error);
                                                                        }
                                                                        else{
                                                                            console.log('sortedlipsum.txt is added in filenames.txt');
                                                                            fs.readFile('./filenames.txt','utf-8',(error,data)=>{
                                                                                if(error){
                                                                                    console.log(error);
                                                                                }
                                                                                else{
                                                                                    for(let file of data.split('\n')){
                                                                                        fs.unlink(file,(error)=>{
                                                                                            if(error){
                                                                                                console.log(error);
                                                                                            }
                                                                                            else{
                                                                                                console.log(`${file} deleted`);
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    });
                                }
                            })
                        }
                    })
                }
            });
        }
    });
}
function splitIntoSentences(str){
    let newstr=str.replaceAll('. ','.\n');
    newstr=newstr.replaceAll('? ','?\n');
    newstr=newstr.replaceAll('! ','!\n');
    newstr=newstr.replaceAll(', ',',\n');
    return newstr.trim().replace(/\n+/g, "\n");;
}
module.exports=fsCallbackMethods;