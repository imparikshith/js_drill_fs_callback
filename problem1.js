const fs=require('node:fs');
function createAndDeleteRandomJSONFiles(path,filecount){
    const checkIfExists=fs.existsSync(path);
    if(checkIfExists){
        console.log('Directory already exists');
    }
    else{
        fs.mkdirSync(path,(error)=>{
            if(error){
                console.log(error);
            }
        });
        console.log('Directory created');
    }

    for(let i=1;i<=filecount;i++){
        fs.writeFile(path+`/${i}-file.json`,`These are the contents of the File ${i}-file.json`,(error)=>{
            if(error){
                console.log(error);
            }
            else{
                console.log(`File ${i}-file.json created `);
                fs.unlink(path+`/${i}-file.json`,(error)=>{
                    if(error){
                        console.log(`Error in deleting ${i}-file.json`);
                    }
                    else{
                        console.log(`${i}-file.json successfully deleted`);
                    }
                })
            }
        });
    }

    setTimeout(function(){fs.rmdir(path,(error)=>{
        if(error){
            console.log(`Error in deleting '${path}' directory`);
        }
        else{
            console.log(`'${path}' directory successfully deleted`);
        }
    })},2000);
}
module.exports=createAndDeleteRandomJSONFiles;